<?php
namespace App\Http\Controllers;

use App\Repositories\Interfaces\NewsRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\View\View;

class NewsController extends Controller
{
    /**
     * @var NewsRepositoryInterface
     */
    private $newsRepository;

    /**
     * @param NewsRepositoryInterface $newsRepository
     */
    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * Get all news and return to view with pagination
     * @param Request $request
     * @return View
     */
    public function showAll(Request $request): View
    {
        $cursor = $request->get('cursor');
        $news = $this->newsRepository->allNews(15, $cursor);
        return view('news', [
            'news' => $news,
            'nextCursor' => $news->nextCursor(),
            'previousCursor' => $news->previousCursor()
        ]);
    }

    /**
     * Get news by ID
     * @param int $id
     * @return View
     */
    public function showById(int $id): View
    {
		$news = $this->newsRepository->getById($id);
        return view('news')->with('news', $news);
    }

    /**
     * Get last three days news
     * @return View
     */
	public function showLastThreeDays(): View
    {
        $news = $this->newsRepository->getLastThreeDays();
        return view('news')->with('news', $news);
    }
}