<?php
namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface NewsRepositoryInterface
{
    /**
     * @param int $id
     * @return Collection
     */
    public function getById(int $id): Collection;

    /**
     * @return Collection
     */
    public function getLastThreeDays(): Collection;

    /**
     * @param int $countPerPage
     * @param string $cursor
     * @return Collection
     */
	public function allNews(int $countPerPage, string $cursor): Collection;
}