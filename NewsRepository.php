<?php
namespace App\Repositories;

use App\Models\News;
use App\Repositories\Interfaces\NewsRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class NewsRepository implements NewsRepositoryInterface
{
    /**
     * @param int $countPerPage
     * @param string $cursor
     * @return Collection
     */
    public function allNews(int $countPerPage, string $cursor): Collection
    {
        return News::query()
            ->orderBy('id', 'DESC')
            ->cursorPaginate($countPerPage, ['*'], 'cursor', $cursor);
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function getById(int $id): Collection
    {
        return News::query()
            ->where('id', '=', $id)
            ->get();
	}

    /**
     * @return Collection
     */
	public function getLastThreeDays(): Collection
    {
        return News::query()
            ->where('created_at','>=',Carbon::now()->subdays(3))
            ->get();
    }
}
